#include<stdio.h>

/* print Fahr - Celc temperatures */

int main(void)
{
    int fahr, celc;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    do {
        celc = 5 * (fahr-32) / 9;
        printf("Fahr: %d\t| Celc: %d\n", fahr, celc);
        fahr = fahr + step;
    } while (fahr <= upper);

    return 0;
}
