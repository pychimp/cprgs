# Note to self:
# Pro tip:
# 	$@ = task name !
#
# -----
# Explict:
#
# hello:
# 	gcc   -Wall     hello.c -o hello.o
# -----
# With Variables:
#
# hello:
# 	$(CC) $(CFLAGS) $@.c    -o $@.o

# Variables

CC = gcc
CFLAGS = -Wall

# Bulids most
all: hello ex1-3_fahrcelc fahrcelc

# This (seems) to work, as calling
# `make all` runs the target `all`
# and since `all` **depepnds** upon
# the other targets (hello, test, etc)
# it (target: all) calls them recursively ! :)


# Builds hello.c
hello:
	$(info --- Building: $@ --- )
	$(CC) $(CFLAGS) $@.c -o $@.o

# Builds ex1-3_fahrcelc:
ex1-3_fahrcelc:
	$(info --- Building: $@ --- )
	$(CC) $(CFLAGS) $@.c -o $@.o

# builds fahrcelc.c
fahrcelc:
	$(info --- Building: $@ --- )
	$(CC) $(CFLAGS) $@.c -o $@.o

# Cleans
clean:
	$(info --- Running: Clean --- )
	rm -rf *.o
